# Imagen base
FROM httpd:2.4

# Copiado de archivos
ADD build/es6-unbundled  /usr/local/apache2/htdocs/

# Puerto que expongo
EXPOSE 80


